
import { WALLET_TYPE } from "./type";

export const clientId = 'BM3U3elbO8XCwDkZ-axIq6qlfLXym5J0FgWiDwEhd2MT7j2MEfx8ZEjRVD4nFVBMBEGZjNUE5NPqMJtOiMT_Qw4';
export const LOGIN_PROVIDER = {
  [WALLET_TYPE.GOOGLE]: 'google',
  [WALLET_TYPE.FACEBOOK]: 'facebook',
  [WALLET_TYPE.TWITTER]: 'twitter',
  [WALLET_TYPE.APPLE]: 'apple',
  [WALLET_TYPE.DISCORD]: 'discord'
}


// export const clientId = 'BOlvW9_5mCDeBQ6hFrh8lyPZ-ReoBGEXnQjWj6-l_8g-VM1TqEHNaYEXAvK5Af15uPVgMzJREdWzUttbO9tcuAs';