import { WALLET_ADAPTERS, CHAIN_NAMESPACES, CustomChainConfig, getEvmChainConfig } from "@web3auth/base";
import { Web3AuthNoModal } from "@web3auth/no-modal";
import { OpenloginAdapter,  OpenloginLoginParams } from "@web3auth/openlogin-adapter";
import { EthereumPrivateKeyProvider } from "@web3auth/ethereum-provider";
import { ethers } from 'ethers';
import { Hex, hexToNumber } from 'viem';

import { WALLET_TYPE } from "./type";
import { clientId, LOGIN_PROVIDER } from './const'

const chainConfig = {
  chainNamespace: CHAIN_NAMESPACES.EIP155,
  rpcTarget:
      'https://eth-sepolia.g.alchemy.com/v2/UJu10EBnVKSI-qO8tp4yarUqVAoNAcrV',
  blockExplorer: 'https://sepolia.etherscan.io/',
  chainId: '0xaa36a7',
  displayName: 'SepoliaTest',
  ticker: 'ETH',
  tickerName: 'ETH'
};

export class Auth {
  web3Auth: Web3AuthNoModal;
  private _signer: any = null;
  get signer(): any {
      return this._signer;
  }
  set signer(value: any) {
      this._signer = value;
  }
  constructor () {

  }
  async getWeb3Auth () {
    if (this.web3Auth) return this.web3Auth;
    const web3Auth =  new Web3AuthNoModal({
      clientId,
      chainConfig,
      enableLogging: true,
      web3AuthNetwork: 'sapphire_devnet'
    });
    const openloginAdapter = new OpenloginAdapter({
      privateKeyProvider: new EthereumPrivateKeyProvider({
        config: {
          chainConfig: getEvmChainConfig(
            hexToNumber(chainConfig.chainId as Hex)
          ) as CustomChainConfig
        }
      })
    });
    web3Auth.configureAdapter(openloginAdapter);
    await web3Auth.init();
    console.log('web3Auth?.connected', web3Auth?.connected, web3Auth);
    this.web3Auth = web3Auth;
    return this.web3Auth;
  };

  async getSigner (walletType?: WALLET_TYPE, options) {
    await this.getWeb3Auth();
    const loginParams = {
      loginProvider: LOGIN_PROVIDER[walletType],
      ...{ redirectUrl: 'http://www.quest.mugeninteractive.co' },
      ...options
    }

    const provider = await this.web3Auth.connectTo<OpenloginLoginParams>(
      WALLET_ADAPTERS.OPENLOGIN,
      loginParams
    );
    
    if (!provider) throw new Error('provider is null');
    const ethersProvider = new ethers.providers.Web3Provider(provider);
    const signer = await ethersProvider.getSigner();
    this._signer = signer
    return { signer };
  };

  isConnect() {
    return this.web3Auth?.connected
  };
  async getUserInfo() {
    return await this.web3Auth.authenticateUser();
  }

  async getAddress() {
    return await this._signer.getAddress()
  }

  async signOut() {
    await this.getWeb3Auth();
    if (this.web3Auth.connected) await this.web3Auth.logout();
  };
}