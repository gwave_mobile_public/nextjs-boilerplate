import { Image } from '../image'
import type { NavBarPropsType } from './type'

export function NavBarH5({onClickLogo, onClickAvatar}: NavBarPropsType) {
  return (
    <div className="absolute top-0 left-0 px-3 flex flex-row justify-between items-center w-full h-12 z-10">
      <div className="w-[88px] h-5" >
        <Image
          alt="mugen"
          height={20}
          onClick={()=>{ onClickLogo('mugen_logo'); }}
          src="/img/mugen_logo.png"
          width={88}
        />
      </div>
      <div className="flex flex-row justify-end items-center w-fit h-fit">
        <div className="flex flex-row items-center w-fit h-fit">
          <Image
            alt="mugen"
            height={21}
            src="/img/point_logo.png"
            width={21}
          />
          <span
            className="text-center align-top text-xs font-Poppins font-semibold whitespace-nowrap text-[#151516] ml-0.5"
          >
            123456
          </span>
        </div>
        <div className="ml-2 w-6 h-6 rounded-[3px]" >
          <Image
            alt="mugen"
            height={24}
            onClick={()=>{ onClickAvatar('avatar'); }}
            src="/img/avatar.png"
            width={24}
          />
        </div>
      </div>
    </div>
  )
}

