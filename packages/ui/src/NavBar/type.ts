export interface NavBarPropsType {
  onClickLogo: (data: string)=>void,
  onClickAvatar: (data: string)=>void
}