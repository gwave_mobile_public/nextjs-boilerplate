import { Image } from '../image'
import type { NavBarPropsType } from './type'

export function NavBarWeb({onClickLogo, onClickAvatar}: NavBarPropsType) {

  

  return (
    <div className="absolute top-0 left-0 bg-white px-8 flex flex-row justify-between items-center w-full h-20 z-10">
      <div className="flex flex-row items-center w-fit h-fit">
        <div className="w-[115px] h-[26px]" >
          <Image
            alt="mugen"
            height={26}
            onClick={()=>{ onClickLogo('mugen_logo'); }}
            src="/img/mugen_logo.png" 
            width={115}
          />
        </div>
        <div className="flex flex-row items-center ml-12 w-fit h-fit">
          <div
            className="bg-[#704dff] rounded-lg px-4 py-2 flex flex-row justify-center items-center w-fit h-fit"
          >
            <span
              className="text-center align-center text-base font-Poppins font-semibold whitespace-nowrap text-white"
            >
              Mssions
            </span>
          </div>
          <div
            className="px-4 py-2 flex flex-row items-center ml-2 w-fit h-fit"
          >
            <span
              className="text-center align-center text-base font-Poppins font-medium whitespace-nowrap text-[#7d7b88]"
            >
              Playground
            </span>
            <div
              className="bg-[#e4e6ee] rounded px-2 flex flex-row justify-center items-center ml-px]w-fit h-fit"
            >
              <span
                className="text-center align-center text-xs font-Poppins font-medium whitespace-nowrap text-[#7d7b88]"
              >
                comming soon
              </span>
            </div>
          </div>
          <div
            className="px-4 py-2 flex flex-row items-center ml-2 w-fit h-fit"
          >
            <span
              className="text-center align-center text-base font-Poppins font-medium whitespace-nowrap text-[#7d7b88]"
            >
              Exchange Center
            </span>
            <div
              className="bg-[#e4e6ee] rounded px-2 flex flex-row justify-center items-center ml-px]w-fit h-fit"
            >
              <span
                className="text-center align-center text-xs font-Poppins font-medium whitespace-nowrap text-[#7d7b88]"
              >
                comming soon
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="flex flex-row justify-end items-center w-fit h-fit">
        <div className="flex flex-row items-center w-fit h-fit">
          <Image
            alt="mugen"
            height={28}
            src="/img/point_logo.png"
            width={28} 
          />
          <span
            className="text-right align-top text-base font-Poppins font-semibold whitespace-nowrap text-[#151516] ml-0.5"
          >
            123456
          </span>
        </div>
        <div className="ml-6 w-8 h-8 rounded-2xl" >
          <Image
            alt="mugen"
            height={32}
            onClick={()=>{ onClickAvatar('avatar'); }}
            src="/img/avatar.png" 
            width={32}
          />
        </div>
      </div>
    </div>
  )
}

