import NextImage from 'next/image';
// TODO: Remove once https://github.com/vercel/next.js/issues/52216 is resolved.
// `next/image` seems to be affected by a default + named export bundling bug.
let ResolvedImage = NextImage;
if ("default" in ResolvedImage) {
  ResolvedImage = (ResolvedImage as unknown as { default: typeof NextImage })
    .default;
}

export const Image = ResolvedImage
 