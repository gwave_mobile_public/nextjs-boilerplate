'use client'
import { useClient } from 'hooks';

export const ResponsiveUIComponent: React.FC = ({Web, H5, ...rest}) => {
  const isPc = useClient();
  return (
    <div className='relative'>
      {isPc 
      ? <Web {...rest}/>
      : <H5 {...rest}/>}
    </div>
  )
}