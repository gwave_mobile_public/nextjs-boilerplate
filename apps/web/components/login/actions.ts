import { Auth, WALLET_TYPE } from 'web3-Auth';


export const initAuth = async () => {
    const web3Auth = new Auth();
    await web3Auth.getWeb3Auth();
    return web3Auth;
}

export const connect = async (type) => {
  try {   
    const web3Auth = await initAuth();
    const isConnect = web3Auth.isConnect()
    if(isConnect){
      await web3Auth.signOut()
    }
    const signer = await web3Auth.getSigner(type);
    const address = await web3Auth.getAddress()
    const userInfo = await web3Auth.getUserInfo()
    console.log('web3Auth', address, userInfo);


  } catch (error) {

  }
}
