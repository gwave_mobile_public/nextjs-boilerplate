'use client'
import { useCallback } from "react"
import { NavBarH5, NavBarWeb } from 'ui';
import { ResponsiveUIComponent } from 'hoc';

export const NavBar: React.FC = () => {
  const onClickAvatar = useCallback((data)=>{
    console.log('avatar', data)
  },[])

  const onClickLogo = useCallback((data)=>{
    console.log('logo', data)
  },[])
  
  return (
    <ResponsiveUIComponent
      H5={NavBarH5}
      Web={NavBarWeb}
      onClickLogo={onClickLogo}
      onClickAvatar={onClickAvatar}
    />
  )
}