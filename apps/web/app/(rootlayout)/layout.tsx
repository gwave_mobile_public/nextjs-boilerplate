'use client';
import React from 'react';
import { NavBar } from '@/components';
import "ui/styles.css";
 
export default function Layout ({ children }: { children: React.ReactNode }) {

  return (
      <div>
        <NavBar />
        {children}
      </div>
  );
}
