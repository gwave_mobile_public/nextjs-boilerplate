'use client';
import Image from 'next/image';
import { connect } from '@/components/login/actions';
import { WALLET_TYPE } from 'web3-auth';

export default function Home() {

  const login = (type) => {
    connect(type);
  }

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24 h-5000">
      <div className="z-10 max-w-5xl w-full items-center justify-between font-mono text-sm lg:flex">
        <p>mugen.play</p>
       
        <div onClick={() => login(WALLET_TYPE.GOOGLE)}>google</div>
        <div onClick={() => login(WALLET_TYPE.TWITTER)}>twitter</div>
        <div onClick={() => login(WALLET_TYPE.APPLE)}>apple</div>
        <div onClick={() => login(WALLET_TYPE.FACEBOOK)}>facebook</div>
        <div onClick={() => login(WALLET_TYPE.DISCORD)}>discord</div>
       
      </div>

    </main>
  )
}
